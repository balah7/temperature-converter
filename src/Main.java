import io.github.balah7.conversor.temperaturas.Celsius;
import io.github.balah7.conversor.temperaturas.Fahrenheit;
import io.github.balah7.conversor.temperaturas.Kelvin;

import java.util.Scanner;

public class Main {

    private static final String SEPARATOR = "--------------------------------";

    public static void main(String[] args) {
        displayWelcomeMessage();

        Scanner scanner = new Scanner(System.in);
        String temperatureType = requestConversionType(scanner);

        while (!temperatureType.equalsIgnoreCase("C") && !temperatureType.equalsIgnoreCase("F") && !temperatureType.equalsIgnoreCase("K")) {
            System.out.println("Invalid temperature, please try again!");
            System.out.println(SEPARATOR);
            temperatureType = requestConversionType(scanner);
        }

        System.out.println("For the next step of the program, we need the temperature value. Please enter it: ");
        double temperatureValue = scanner.nextDouble();

        displayConversionResult(temperatureType, temperatureValue);

        scanner.close();
    }

    private static void displayWelcomeMessage() {
        System.out.println("""
                ╔═════════════════════════════╗
                ║   Temperature Converter     ║
                ╚═════════════════════════════╝
                """);
        System.out.println("Welcome to the temperature conversion program!");
        System.out.println(SEPARATOR);
    }

    private static String requestConversionType(Scanner scanner) {
        System.out.println("To begin, I need to know which type of temperature you want to convert.");
        System.out.println("Enter F for Fahrenheit, C for Celsius, or K for Kelvin: ");
        System.out.println(SEPARATOR);
        return scanner.next();
    }

    private static void displayConversionResult(String temperatureType, double temperatureValue) {
        switch (temperatureType.toUpperCase()) {
            case "C":
                Celsius celsius = new Celsius(temperatureValue);
                displayTemperatures(temperatureValue, celsius.toFahrenheit(temperatureValue), celsius.toKelvin(temperatureValue));
                break;

            case "F":
                Fahrenheit fahrenheit = new Fahrenheit(temperatureValue);
                displayTemperatures(fahrenheit.toCelsius(temperatureValue), temperatureValue, fahrenheit.toKelvin(temperatureValue));
                break;

            case "K":
                Kelvin kelvin = new Kelvin(temperatureValue);
                displayTemperatures(kelvin.toCelsius(temperatureValue), kelvin.toFahrenheit(temperatureValue), temperatureValue);
                break;
        }
    }

    private static void displayTemperatures(double celsius, double fahrenheit, double kelvin) {
        System.out.printf("""
                -----------------------------------
                |          Temperatures           |
                |---------------------------------|
                |  Celsius   | Fahrenheit | Kelvin |
                |------------|------------|--------|
                |   %.2f°C  |   %.2f°F  | %.2fK |
                -----------------------------------
                %n""", celsius, fahrenheit, kelvin);
    }
}
