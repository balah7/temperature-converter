package io.github.balah7.conversor.temperaturas;

public class Celsius extends Temperatura{

    public Celsius(double valor) {
        super(valor);
    }

    public double toFahrenheit(double celsius) {
        return (celsius * 9 / 5) + 32;
    }

    public double toKelvin(double celsius) {
        return celsius + 273.15;
    }
}
