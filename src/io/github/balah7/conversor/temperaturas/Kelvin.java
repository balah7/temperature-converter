package io.github.balah7.conversor.temperaturas;

public class Kelvin extends Temperatura{

    public Kelvin(double valor) {
        super(valor);
    }

    public double toCelsius(double kelvin) {
        return kelvin - 273.15;
    }

    public double toFahrenheit(double kelvin) {
        return (kelvin - 273.15) * 9 / 5 + 32;
    }
}
