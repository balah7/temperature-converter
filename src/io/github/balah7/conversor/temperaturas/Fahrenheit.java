package io.github.balah7.conversor.temperaturas;

public class Fahrenheit extends Temperatura{

    public Fahrenheit(double valor) {
        super(valor);
    }

    public double toCelsius(double fahrenheit) {
        return (fahrenheit - 32) * 5 / 9;
    }

    public double toKelvin(double fahrenheit) {
        return  (fahrenheit - 32) * 5 / 9 + 273.15;
    }
}
