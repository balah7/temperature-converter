# Temperature Converter ☁️

Bem-vindo ao meu projeto de Conversor de Temperatura! Este é um simples programa em Java que eu criei para praticar minhas habilidades na linguagem. Por favor, esteja ciente de que esta é uma das minhas primeiras tentativas de usar Java, então o código pode não estar perfeito e é apenas para fins de aprendizado.

## Sobre o Projeto

O Conversor de Temperatura permite que você converta temperaturas entre Celsius, Fahrenheit e Kelvin. Ele inclui uma interface de linha de comando amigável com uma exibição visualmente atraente das conversões de temperatura feita na língua inglesa.

## Como Começar

### Pré-requisitos
- Kit de Desenvolvimento Java (JDK) instalado em sua máquina.

### Instalação
1. Clone o repositório:
   ```bash
     git clone https://github.com/seuusuario/conversor-temperatura.git
     cd conversor-temperatura
     cd src
     javac Main.java
     java Main```

Acabou ;) 
